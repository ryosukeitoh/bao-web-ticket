#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json,time
import cgi
import codecs
from http import cookies
import io,sys,os
import hashlib

sys.path.append("/usr/lib/cgi-bin/")

import entrance_ticket
import counter3


def show_html_header():
    print("Content-type: text/html\n\n")
    print('<html><meta charset=utf-8><head>')
    print("<title>電子整理券管理システム</title>")
    print("</head><body>Sending request...")
    
def show_html_end():
    print('Completed!! <a href="/ticket/controll_ticket.html">Back</a>')
    print("</body></html>")



if __name__ == "__main__":
    sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')
    BT = entrance_ticket.BAO_ticket()
    show_html_header()
    form = cgi.FieldStorage()
    nID  = int(form.getvalue("ID"))
    #nID = 5
    BT.read_current()
    BIND = BT.BL["ID"].index(nID)
    w = open(BT.tfile,"w")
    for i in range(len(BT.BL["hashid"])):
        if i != BIND:
            w.write("%s %s %s %s %s %s %s\n"%(BT.BL["hashid"][i],
                                              BT.BL["ID"][i],
                                              BT.BL["req_date"][i],
                                              BT.BL["rec_date"][i],
                                              BT.BL["ac_num"][i],
                                              BT.BL["checkin"][i],
                                              BT.BL["canceled"][i]))
        else:
            rec_date = BT.make_ymd(time.localtime())
            w.write("%s %s %s %s %s %s %s\n"%(BT.BL["hashid"][i],
                                              BT.BL["ID"][i],
                                              BT.BL["req_date"][i],
                                              rec_date,
                                              BT.BL["ac_num"][i],
                                              1,
                                              BT.BL["canceled"][i]))
            m,n1,n2,n3,n4 = counter3.read_current()
            for j in range(BT.BL["ac_num"][i]):
                n1 += 1
                counter3.write_time("p")
            n4 = counter3.count_group()
            counter3.write_current(m,n1,n2,n3,n4)
            
                    
    w.close()  
    show_html_end()
    BT.remove_lock()
