#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json,time
import cgi
import codecs
import io,sys,os
from http import cookies
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')
sys.path.append("/usr/lib/cgi-bin/")
import entrance_ticket


fontc = "white"
#fontc = "blue"

def show_html_header():
    print("Content-type: text/html\n\n")
    print("<html><meta charset=utf-8><head>")
    print("<title>美星天文台電子整理券</title>")
    print("</head>")
    print("<body><font color='%s'>"%fontc)

def set_time():
    t1  = "18:00:00"
    t2  = "20:00:00"
    tt  = time.localtime()
    nt1 = "%04d/%02d/%02d-%s"%(tt[0],tt[1],tt[2],t1)
    nt2 = "%04d/%02d/%02d-%s"%(tt[0],tt[1],tt[2],t2)
    return nt1,nt2

def get_current_num(timeList):
    BT = entrance_ticket.BAO_ticket()
    BT.read_current()
    BT.countup_allreservation()
    L = {}
    for date in BT.allres.keys():
        ex_d = BT.read_bnum()
        if date in ex_d.keys():maxn = ex_d[date]
        else:                  maxn = BT.defoult_max_num
        L[date] = maxn
    cf = False
    for t in timeList:
        if t in ex_d.keys():cf = True

    ck   = cookies.SimpleCookie(os.environ.get('HTTP_COOKIE',''))
    reg  = False
    if "baotickethashid" in ck.keys():
        hid = ck["baotickethashid"].value
        if hid in BT.BL["hashid"]:
            reg = True
    BT.remove_lock()
    return BT.allres,L,cf,reg

def check_bookratio(resL,mxL,date):
    if date not in resL.keys():
        return 0
    #r = resL[date]/mxL[date]
    r = mxL[date] - resL[date]
    BT = entrance_ticket.BAO_ticket()
    if os.path.exists(BT.elock):
        return 2
    if r > 10:
        return 0
    elif r <= 10 and resL[date] < mxL[date]:
        return 1
    else:
        return 2
    
def show_error():
    print("エラーが発生しました。お時間をおいて再度お試しください。<br>")
    print("<a href='https://www.bao.city.ibara.okayama.jp/?page_id=59' target='_blank'>")
    print("</a>お問い合わせ先はこちら<br>")
    print('<button type="button" onclick="history.back()">戻る</button>')
                        
if __name__ == "__main__":
    show_html_header()
    try:
        t1,t2 = set_time()
        cn,mx,cf,ckf = get_current_num([t1,t2])
        LS = {}
        for d in [t1,t2]:
            LS[d] = check_bookratio(cn,mx,d)
    except:
        show_error()
        sys.exit()
    bc = {0:"aqua",1:"orange",2:"gray"}
    #tc = {0:"〇",1:"△",2:"×"}
    #tc  = {0:"空きあり",1:"残り%s名まで",2:"定員に達しました"}
    tc  = {0:"空きあり",1:"残りわずか",2:"定員に達しました"}
    if cf:
        print("<p>人数を入力し、日時を確認したうえで発行ボタンをクリックして下さい。</p>")
        print("<form action='redirect_ticket.py' method='post'>")
        print("<input type='hidden' name='mode' value='write'>")
        print("<table><tr>")
        print("<td><font color=%s>人数</td>"%fontc)
        print("<td align=center><font color=%s>合計 <select id='num' name='num'>"%fontc)
        print("<option value='0'>0人(キャンセル)</option>")
        for i in range(1,20):
            print("<option value='%s'>%s人</option>"%(i,i))
        print("</select>")
        print("(乳幼児を含む)</td></tr>")
        print("<td><font color=%s>日付</td>"%fontc)
        print("<td align=center><font color=%s>本日 %s 月 %s 日</td></tr>"%(fontc,
                                                                            t1.split("/")[1],
                                                                            t1.split("/")[2].split("-")[0]))
        print("<tr><td><font color='%s'>空き状況</font></td>"%fontc)

        #if LS[t1] == 1:
        #    tcc = mx[t1]-cn[t1]
        #    print("<td bgcolor=%s align=center>%s</td>"%(bc[LS[t1]],tc[LS[t1]]%tcc))
        #else:
        #    print("<td bgcolor=%s align=center>%s</td>"%(bc[LS[t1]],tc[LS[t1]]))
        print("<td bgcolor=%s align=center>%s</td>"%(bc[LS[t1]],tc[LS[t1]]))
        print("</tr>")
        #print("<td><font color=%s></td>"%fontc)
        print("<td colspan=2>")
        ch = "<button style='width:100%;color:blue;background-color:white ;font-size:20'"
        if ckf:
            print(ch)
            print(" name='date' value='%s'>変更</button>"%t1)
        elif LS[t1] == 2:
            print("<font color=%s>現在、新規発行ができません"%fontc)
        else:
            print(ch)
            print(" name='date' value='%s'>整理券発行</button>"%t1)

        print("</form></td></tr>")
        print("</tr></table>")
        print("<br>")
        if ckf:
            print("<form action='redirect_ticket.py' method='post'><button name='mode' value='confirm'>整理券を確認</button></form>")
    else:
        print("本日の夜間公開はありません。<br>")
        print("整理券の発行は、夜間公開のある日のみ可能です。<br>")
    print("</font></body></html>")
        
