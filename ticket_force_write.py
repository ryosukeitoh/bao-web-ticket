#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json,time
import cgi
import codecs
from http import cookies
import io,sys,os
import hashlib

sys.path.append("/usr/lib/cgi-bin/")

import entrance_ticket

def show_html_header():
    print("Content-type: text/html\n\n")
    print("<html><meta charset=utf-8><head>")
    print("<title>美星天文台電子整理券</title>")
    print("</head><body>")
                

def rewrite_db(form):
    print("Sending requests....")
    BT       = entrance_ticket.BAO_ticket()
    BT.read_current()
    BT.countup_allreservation()
    BT.ex_d  = BT.read_bnum()
    BT.date  = form.getvalue("date")
    BT.num   = int(form.getvalue("num"))
    ID       = int(form.getvalue("ID"))
    BT.checkin  = form.getvalue("cin")
    BT.canceled = form.getvalue("can")

    BT.cur_num = BT.allres[BT.date]
    BT.maxn    = BT.ex_d[BT.date]
    force_flag  = int(form.getvalue("force"))
    
    BIND     = BT.BL["ID"].index(ID)
    BT.hid   = BT.BL["hashid"][BIND]
    if BT.date not in BT.allres.keys():BT.allres[BT.date] = 0
    if BT.date not in BT.ex_d.keys():BT.ex_D[BT.date]  = BT.defoult_max_num 
    if BT.allres[BT.date] - BT.BL["ac_num"][BIND] + BT.num > BT.ex_d[BT.date] and force_flag==0:
        print("人数制限を超えています。書き換えれません。<br>")
        print('<a href="/ticket/controll_ticket.html">Back</a>')
        print("</body></html>")
    else:
        BT.change_reservation()
        print('Completed!! <a href="/ticket/controll_ticket.html">Back</a>')
        print("</body></html>")
    BT.remove_lock()
    
def rewrite_schejule(form):
    print("Sending requests....")
    BT = entrance_ticket.BAO_ticket()

    L  = form.getvalue("schedule")
    cL = L.replace("\r\n","\n").replace("\r","\n")
    w  = open(BT.dfile,"w")
    w.write(cL)
    #for l in L.split("\n"):
    #   w.write("%s\n"%l)
    w.close()
    print('Completed!! <br>')
    print('<a href="/cgi-bin/ticket/write_schedule.py">Back</a>')
    print("</body></html>")

    
def write_db(form):
    print("Sending requests....")
    BT          = entrance_ticket.BAO_ticket()
    BT.read_current()
    BT.countup_allreservation()
    BT.ex_d     = BT.read_bnum()
    BT.date     = form.getvalue("date")
    BT.num      = int(form.getvalue("num"))
    BT.checkin  = form.getvalue("cin")
    BT.canceled = form.getvalue("can")
    force_flag  = int(form.getvalue("force"))
    if len(BT.BL["ID"])!=0:BT.ID = max(BT.BL["ID"])+1
    else:BT.ID = 1
    dat        = str(BT.date)+str(BT.num)+str(BT.ID)
    BT.hid     = hashlib.sha256(dat.encode()).hexdigest()
    if BT.date not in BT.allres.keys():BT.allres[BT.date] = 0
    if BT.date not in BT.ex_d.keys():BT.ex_d[BT.date]  = BT.defoult_max_num 
    BT.cur_num = BT.allres[BT.date]
    BT.maxn    = BT.ex_d[BT.date]
    if BT.allres[BT.date] + BT.num > BT.ex_d[BT.date] and force_flag==0:
        print("人数制限を超えています。書き換えれません。<br>")
        print('<a href="/ticket/controll_ticket.html">Back</a>')
        print("</body></html>")
    else:
        BT.write_new_reservation()
        print('Completed!! <br>')
        print('<h3>IDは %s です</h3>'%BT.ID)
        print('<a href="/ticket/controll_ticket.html">Back</a>')
        print("</body></html>")

    
if __name__ == "__main__":
    sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')
    form = cgi.FieldStorage()
    mode = form.getvalue("mode")
    show_html_header()
    if   mode == "change":  rewrite_db(form)
    elif mode == "new":     write_db(form)
    elif mode == "schedule":rewrite_schejule(form)
    
    
