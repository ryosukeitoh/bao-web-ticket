#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json,time
import cgi
import codecs
from http import cookies
import io,sys,os
import hashlib

sys.path.append("/usr/lib/cgi-bin/")

import entrance_ticket

def show_html_header():
    print("Content-type: text/html\n\n")
    print('<html><meta charset=utf-8><head>')
    print("<title>電子整理券管理システム</title>")
    print("</head><body>")
    
def show_html_end():
    print('<br><a href="/ticket/controll_ticket.html">Back</a>')
    print("</body></html>")


def show_current(L,ind,dL):

    print('<form action="redirect_ticket_force.py" method="post">')
    print("<input type='hidden' name='mode' value='change'>")
    print("<input type='hidden' name='ID' value='%s'>"%L["ID"][ind])
    print("<table><tr><th>Column</th><th></th><th>Value</th></tr>")
    print("<tr><td>ID</td><td>:</td><td>%s</td></tr>"%L["ID"][ind])
    print("<tr><td>人数</td><td>:</td><td>")
    print('<select name="num">')
    for i in range(0,51):
        if i == int(L["ac_num"][ind]):
            print('<option value="%s" selected="selected">%s</option>'%(i,i))
        else:
            print('<option value="%s">%s</option>'%(i,i))
    print("</select></td></tr>")
    print("<tr><td>予約時間</td><td>:</td><td>")
    print('<select name="date">')
    sdL = sorted(dL.keys())
    for d in sdL:
        if d == L["req_date"][ind]:print('<option value="%s" selected="selected">%s</option>'%(d,d))
        else:                      print('<option value="%s">%s</option>'%(d,d))
    if L["req_date"][ind] not in sdL:
        print('<option value="%s" selected="selected">%s</option>'%(L["req_date"][ind],L["req_date"][ind]))
    print("</select></td></tr>")
    print("<tr><td>更新時刻</td><td>:</td><td>%s</td></tr>"%L["rec_date"][ind])
    print("<tr><td>Check-in</td><td>:</td><td>")
    print('<select name="cin">')
    if L["checkin"][ind] == 0:
        print('<option value="0" selected="selected">0 (未到着)</option>')
        print('<option value="1" >1 (受付済)</option>')
    else:
        print('<option value="0" >0 (未到着)</option>')
        print('<option value="1" selected="selected">1 (受付済)</option>')
    print("</select></td></tr>")
    print("<tr><td>Canceled</td><td>:</td><td>")
    print('<select name="can">')
    if L["canceled"][ind] == 0:
        print('<option value="0" selected="selected">0 (予約有効)</option>')
        print('<option value="1" >1 (キャンセル)</option>')
    else:
        print('<option value="0" >0 (予約有効)</option>')
        print('<option value="1" selected="selected">1 (キャンセル)</option>')    
    print("</select></td></tr>")
    print("<tr><td>Cookie</td><td>:</td><td>%s</td></tr>"%L["hashid"][ind])
    print("<tr><td>権限</td><td>:</td><td>")
    print('<input type="radio" name="force" value="0" checked="checked">通常')
    print('<input type="radio" name="force" value="1">強制書換')
    print("</td></tr>")
    print("</table>")
    print('<input type="submit" value="反映"></form>')
    
if __name__ == "__main__":
    sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')
    BT   = entrance_ticket.BAO_ticket()
    ex_d = BT.read_bnum()
    show_html_header()
    form = cgi.FieldStorage()
    nID  = int(form.getvalue("ID"))
    BT.read_current()
    BIND = BT.BL["ID"].index(nID)

    show_current(BT.BL,BIND,ex_d)
    show_html_end()
    BT.remove_lock()
