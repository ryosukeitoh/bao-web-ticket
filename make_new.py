#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json,time
import cgi
import codecs
from http import cookies
import io,sys,os
import hashlib

sys.path.append("/usr/lib/cgi-bin/")

import entrance_ticket

def show_html_header():
    print("Content-type: text/html\n\n")
    print('<html><meta charset=utf-8><head>')
    print("<title>電子整理券管理システム</title>")
    print("</head><body>")
    
def show_html_end():
    print('<br><a href="/ticket/controll_ticket.html">Back</a>')
    print("</body></html>")


def show_state(date,cn,mn):
    print('<form action="redirect_ticket_force.py" method="post">')
    print("<input type='hidden' name='mode' value='new'>")
    print("<input type='hidden' name='date' value='%s'>"%date)
    print("<table><tr><th>Column</th><th></th><th>Value</th></tr>")
    print("<tr><td>予約時間</td><td>:</td><td>%s</td></tr>"%date)
    print("<tr><td>現在の登録者数</td><td>:</td><td>%s/%s</td>"%(cn,mn))
    print("<tr><td>人数</td><td>:</td><td>")
    print('<select name="num">')
    for i in range(0,51):print('<option value="%s">%s</option>'%(i,i))
    print("</select></td></tr>")
    print("<tr><td>Check-in</td><td>:</td><td>")
    print('<select name="cin">')
    print('<option value="0" selected="selected">0 (未到着)</option>')
    print('<option value="1" >1 (受付済)</option>')
    print("</select></td></tr>")
    print("<tr><td>Canceled</td><td>:</td><td>")
    print('<select name="can">')
    print('<option value="0" selected="selected">0 (予約有効)</option>')
    print('<option value="1" >1 (キャンセル)</option>')
    print("</select></td></tr>")
    print("<tr><td>権限</td><td>:</td><td>")
    print('<input type="radio" name="force" value="0" checked="checked">通常')
    print('<input type="radio" name="force" value="1">強制書換')
    print("</td></tr>")
    print("</table>")
    print('<input type="submit" value="反映"></form>')
    
if __name__ == "__main__":
    sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')
    BT   = entrance_ticket.BAO_ticket()
    ex_d = BT.read_bnum()
    BT.read_current()
    BT.countup_allreservation()
    show_html_header()
    form = cgi.FieldStorage()
    date = form.getvalue("date")


    if date in BT.allres.keys():cnum = BT.allres[date]
    else:                       cnum = 0
    if date in ex_d.keys():     cmax = ex_d[date]
    else:                       cmax = BT.defoult_max_num
    show_state(date,cnum,cmax)
    show_html_end()
    BT.remove_lock()
