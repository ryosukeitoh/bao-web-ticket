#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json,time
import cgi
import codecs
from http import cookies
import io,sys,os
import hashlib

sys.path.append("/usr/lib/cgi-bin/")

import entrance_ticket

    
if __name__ == "__main__":
    sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')
    BT       = entrance_ticket.BAO_ticket()
    form     = cgi.FieldStorage()
    BT.mode  = form.getvalue("mode")

    if BT.mode == "status-all":
        BT.get_status()
        BT.show_json()
    elif BT.mode == "status":
        BT.get_status_today()
        BT.show_json()
