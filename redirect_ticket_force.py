#! /usr/bin/env python
# -*- coding: utf-8 -*-

import cgi,sys,time,os



class status():

    def __init__(self):

        self.htmlhead = """
        <html>
        <head>
        <meta http-equiv="refresh" content="0; URL='%s'" />
        <title>redirect</title>
        </head>
        <body>
        """
        self.htmlend = "</body></html>"


    def show(self):
        print "Content-Type: text/html\n"
        url = "ticket_force_write.py?"
        for n in self.c:
            val  = self.c.getvalue(n)
            if isinstance(val, list):
                val = val[0]
            url += str(n)+"="+val+"&"
        new = url[0:-1]
        print self.htmlhead%new
        print new,"<br>"
        print "Sending requests..."
        print self.htmlend        


    def show_end(self):
        print "Content-Type: text/html\n"
        print "Finished"


if __name__ == "__main__":
    st    = status()
    st.c  = cgi.FieldStorage()
    st.show()

