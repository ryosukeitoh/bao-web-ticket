#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json,time
import cgi
import codecs
#import cookies
from http import cookies
import io,sys,os
import hashlib

from operator import itemgetter, attrgetter



class BAO_ticket():

    def __init__(self):
        self.tz       = 9.0 # time zone correction
        self.lockfile = ".lock_ticket.txt"
        cnt = 0
        while True:
            if os.path.exists(self.lockfile) ==False:
                break
            time.sleep(0.1)
            cnt += 1
            if 50 < cnt:break
            
        os.system("touch %s"%self.lockfile)
        self.tfile = "ticket.txt"
        self.dfile = "ticket_schedule.txt"
        self.elock = ".lock_new_ticket.txt"
        self.eventL= ["2020/12/14-18:00:00"]
        self.defoult_max_num = 50
        self.keys  = ["hashid","ID","req_date","rec_date",
                      "ac_num","checkin","canceled"]
        self.form  = cgi.FieldStorage()
        self.mode  = self.form.getvalue("mode")
        self.fontc  = "white"
        if __name__ == "__main__" and self.mode == "write":
            self.date  = self.form.getvalue("date")
            self.num   = int(self.form.getvalue("num"))
            self.checkin  = self.form.getvalue("cin")
            self.canceled = self.form.getvalue("can")
            
            if self.canceled is None:self.canceled= 0
            if self.checkin  is None:self.checkin = 0
            self.read_current()
            self.starttime = "13:45:00"
            self.endtime   = "21:30:00"
            self.h_width   = 12.0 #hour
            self.check_time()
            self.check_end_time()     
            ex_d = self.read_bnum()
            if self.date in ex_d.keys():self.maxn = ex_d[self.date]
            else:                       self.maxn = self.defoult_max_num
            # check cookies
            ck   = cookies.SimpleCookie(os.environ.get('HTTP_COOKIE',''))
            self.reg = False 
            if "baotickethashid" in ck.keys():
                self.hid = ck["baotickethashid"].value
                if self.hid in self.BL["hashid"]:
                    self.reg = True
            if self.reg == False:
                if len(self.BL["ID"])!=0:self.ID  = max(self.BL["ID"])+1
                else:self.ID = 1
                dat      = str(self.date)+str(self.num)+str(self.ID)
                self.hid = hashlib.sha256(dat.encode()).hexdigest()
                self.reg = False
            self.check_num()
        if __name__ == "__main__" and self.mode == "confirm":
            # check cookies
            self.read_current()
            ck   = cookies.SimpleCookie(os.environ.get('HTTP_COOKIE',''))
            self.reg = False 
            if "baotickethashid" in ck.keys():
                self.hid = ck["baotickethashid"].value
                if self.hid in self.BL["hashid"]:
                    self.reg = True

    def __del__(self):
        if os.path.exists(self.lockfile):
            os.system("rm %s"%self.lockfile)        
    
    def show_json(self):
        res = {"result":self.JL,"date":self.settime,
               "num":self.allres,"max":self.ex_d}
        print("Content-type: application/json\n\n")
        print(json.JSONEncoder().encode(res))

    def show_json_numerror(self):
        response={"error":"True",
                  "state":"MAXNUM"}
        print("Content-type: application/json\n\n")
        print(json.JSONEncoder().encode(response))

    def show_html_header(self):
        print("Content-type: text/html\n\n")
        print("<html><meta charset=utf-8><head>")
        print("<title>美星天文台電子整理券</title>")
        print("</head><body><font color='%s'>"%self.fontc)
        
    def show_html_end(self):
        print("</font></body></html>")

    def show_new_reservation(self):
        print("<h3>新規の整理券を発行しました。</h3>")
        #print("受付時刻: %s"%self.make_ymd(time.localtime()))
        print("<table>")
        print("<tr><td><font color='%s'>受付番号</font></td>"%self.fontc)
        print("<td align=center><font color='%s'>%05d</font></td></tr>"%(self.fontc,self.ID))
        print("<tr><td><font color='%s'>日時</font></td>"%self.fontc)
        print("<td align=center><font color='%s'>%s</font></td></tr>"%(self.fontc,self.date.replace("-18:00:00","")))
        print("<tr><td><font color='%s'>参加人数</font></td>"%self.fontc)
        print("<td align=center><font color='%s'>%s人</font></td></tr>"%(self.fontc,self.num))
        print("</table>")
        print("本画面が入館時に必要となります。")
        print("必ず本画面を<font color='red'>保存(スクリーンショットもしくは印刷)し</font>、")
        print("天文台受付までご持参ください。<br>")
        print("ID: %05d<br>"%(self.ID))
        self.get_twitime()
        print("暗くなって星が見え始めるのは %s:%s 頃からです。<br>"%(self.twi[0],self.twi[1]))       
        print("夜間は冷え込みます。どうぞ防寒着をご用意ください。<br>")
        print('<button type="button" onclick="history.back()">戻る</button>')
        self.set_cookie()

    def show_new_reservation4event(self):
        print("<h3>予約券を発行しました。</h3>")
        print("<table>")
        print("<tr><td><font color='%s'>受付番号</font></td>"%self.fontc)
        print("<td align=center><font color='%s'>%05d</font></td></tr>"%(self.fontc,self.ID))
        print("<tr><td><font color='%s'>日時</font></td>"%self.fontc)
        print("<td align=center><font color='%s'>%s</font></td></tr>"%(self.fontc,self.date.replace("-18:00:00","")))
        print("<tr><td><font color='%s'>参加人数</font></td>"%self.fontc)
        print("<td align=center><font color='%s'>%s人</font></td></tr>"%(self.fontc,self.num))
        print("</table>")
        print("本画面が入館時に必要となります。")
        print("必ず本画面を<font color='red'>保存(スクリーンショットもしくは印刷)し</font>、天文台受付までご持参ください。<br>")
        print("夜間は冷え込みます。どうぞ防寒着をご用意ください。<br>")
        print('<button type="button" onclick="history.back()">戻る</button>')
        
    def show_change_reservation(self):
        print("<h3>整理券を変更しました。</h3>")
        #print("受付時刻: %s"%self.make_ymd(time.localtime()))
        if self.num == 0:
            print("整理券をキャンセルしました。")
        print("<table>")
        print("<tr><td><font color='%s'>受付番号</font></td>"%self.fontc)
        print("<td align=center><font color='%s'>%05d</font></td></tr>"%(self.fontc,self.oID))
        print("<tr><td><font color='%s'>日時</font></td>"%self.fontc)
        print("<td align=center><font color='%s'>%s</font></td></tr>"%(self.fontc,self.date.replace("-18:00:00","")))
        print("<tr><td><font color='%s'>参加人数</font></td>"%self.fontc)
        print("<td align=center><font color='%s'>%s人</font></td></tr>"%(self.fontc,self.num))
        print("</table>")
        if self.num != 0:
            print("本画面が入館時に必要となります。")
            print("必ず本画面を<font color='red'>保存(スクリーンショットもしくは印刷)し</font>、天文台受付までご持参ください。<br>")
        print('<button type="button" onclick="history.back()">戻る</button>')

    def show_reservation(self):
        print("<h3>下記の通り整理券を発行しています。</h3>")
        print("<table>")
        print("<tr><td><font color='%s'>受付番号</font></td>"%self.fontc)
        print("<td align=center><font color='%s'>%05d</font></td></tr>"%(self.fontc,self.ID))
        print("<tr><td><font color='%s'>日時</font></td>"%self.fontc)
        print("<td align=center><font color='%s'>%s</font></td></tr>"%(self.fontc,self.date.replace("-18:00:00","")))
        print("<tr><td><font color='%s'>参加人数</font></td>"%self.fontc)
        print("<td align=center><font color='%s'>%s人</font></td></tr>"%(self.fontc,self.num))
        print("</table>")
        if self.num != 0:
            print("本画面が入館時に必要となります。")
            print("必ず本画面を<font color='red'>保存(スクリーンショットもしくは印刷)し</font>、天文台受付までご持参ください。<br>")
        print('<button type="button" onclick="history.back()">戻る</button>')

    def show_no_reservation(self):
        print("<h3>お客様の整理券は未発行です。</h3>")
        print('<button type="button" onclick="history.back()">戻る</button>')
        
    def show_time_limit(self):
        hh,mm,ss = list(map(int,self.starttime.split(":")))
        print("電子整理券の配布は当日%02d:%02dからです。時間まで少々お待ちください<br>"%(hh,mm))
        print('<button type="button" onclick="history.back()">戻る</button>')

    def show_end_limit(self):
        hh,mm,ss = list(map(int,self.endtime.split(":")))
        print("電子整理券の配布は当日%02d:%02dまでです.<br>"%(hh,mm))
        print('<button type="button" onclick="history.back()">戻る</button>')

    def show_num_limit(self):
        print("既に定員に達しておりますので、お申込みの人数では整理券を発行できません。大変申し訳ありません。<br>")
        print('<button type="button" onclick="history.back()">戻る</button>')

    def show_enter_number(self):
        print("人数をご入力ください<br>")
        print('<button type="button" onclick="history.back()">戻る</button>')


    def rm_f2(self,val):
        import ephem
        sr = "%s"%(ephem.Date(val + self.tz/24.0+10.0/60.0/24.0))
        return sr.split(".")[0]
                  
    
    def get_twitime(self):
        import ephem_BAO
        BAO       = ephem_BAO.bao_planets()
        BAO.set_time("today")
        BAO.allplanets()
        BAO.calc_sun_and_moon()
        BAO.calc_all_rise_and_set_time()
        self.twi  = self.rm_f2(BAO.ctwi_end).split()[-1].split(":")

        
    def remove_lock(self):
        os.system("rm %s"%self.lockfile)
        
    def read_bnum(self):
        k = open(self.dfile,"r")
        L = {}
        r = k.read().split("\n")
        k.close()
        for l in r:
            d = l.split()
            if len(d) != 2:continue
            L[d[0]] = int(d[1])
        return L

    def read_current(self):
        L = {}
        for k in self.keys:L[k] = []
        k = open(self.tfile,"r")
        r = k.read().split("\n")
        k.close()
        for l in r:
            d = l.split()
            if len(d) <= 1:continue
            for i in range(len(self.keys)):
                try:
                    L[self.keys[i]].append(int(d[i]))
                except:
                    L[self.keys[i]].append(d[i])
        self.BL = L
        
    def write_new_reservation(self):
        k = open(self.tfile,"a")
        rec_date = self.make_ymd(time.localtime())
        k.write("%s %s %s %s %s %s %s\n"%(self.hid,self.ID,self.date,rec_date,
                                          self.num,self.checkin,self.canceled))
        k.close()
        if self.cur_num + self.num >= self.maxn:
            os.system("touch %s"%self.elock)
            
    def change_reservation(self):
        os.system("cp %s %s"%(self.tfile,self.tfile+".back"))
        w = open(self.tfile,"w")
        for i in range(len(self.BL["hashid"])):
            if self.BL["hashid"][i] != self.hid:
                w.write("%s %s %s %s %s %s %s\n"%(self.BL["hashid"][i],
                                                  self.BL["ID"][i],
                                                  self.BL["req_date"][i],
                                                  self.BL["rec_date"][i],
                                                  self.BL["ac_num"][i],
                                                  self.BL["checkin"][i],
                                                  self.BL["canceled"][i]))
            else:
                if int(self.num) == 0 or int(self.canceled) == 1:
                    self.canceled = 1
                    self.num      = 0

                self.oID = self.BL["ID"][i]
                rec_date = self.make_ymd(time.localtime())
                w.write("%s %s %s %s %s %s %s\n"%(self.hid,self.BL["ID"][i],
                                                  self.date,rec_date,
                                                  self.num,self.checkin,
                                                  self.canceled))
                
        w.close()
        if self.cur_num + self.num >= self.maxn:
            os.system("touch %s"%self.elock)
            
    def check_reservation(self):
        for i in range(len(self.BL["hashid"])):
            if self.BL["hashid"][i] == self.hid:
                self.ID   = self.BL["ID"][i]
                self.date = self.BL["req_date"][i]
                self.num  = self.BL["ac_num"][i]
                
        
    def check_time(self):
        nt  = time.time()
        lt  = time.localtime(nt)
        td  = self.date.split("-")[0]
        ct  = "%04d/%02d/%02d"%(lt[0],lt[1],lt[2])
        st  = list(map(int,self.starttime.split(":")))
        tst = time.mktime((lt[0],lt[1],lt[2],st[0],st[1],st[2],0,0,0))
        if self.date in self.eventL:
            self.timeflag = True
        elif tst <= nt and nt < tst + self.h_width*3600.0 and ct == td:
            self.timeflag = True
        else:
            self.timeflag = False
            
    def check_end_time(self):
        nt  = time.time()
        lt  = time.localtime(nt)
        td  = self.date.split("-")[0]
        ct  = "%04d/%02d/%02d"%(lt[0],lt[1],lt[2])
        st  = list(map(int,self.endtime.split(":")))
        tst = time.mktime((lt[0],lt[1],lt[2],st[0],st[1],st[2],0,0,0))
        if self.date in self.eventL:
            self.endtimeflag = True
        elif nt < tst and ct == td:
            self.endtimeflag = True
        else:
            self.endtimeflag = False
            
    def check_num(self):
        cnt = 0
        for i in range(len(self.BL["req_date"])):
            if self.date == self.BL["req_date"][i] and \
               self.hid != self.BL["hashid"][i]:
                cnt += self.BL["ac_num"][i]
        self.cur_num = cnt
        #if self.maxn < cnt + self.num and self.num !=0:
        #elif self.maxn-10 < cnt and num != 0:
        #   self.numflag = True
        #if self.maxn < cnt + self.num and self.num !=0:
        #    self.numflag = False
        if self.maxn <= cnt and self.num != 0:
            self.numflag = False
        else:self.numflag = True

    def countup_allreservation(self):
        self.allres = {}
        for i in range(len(self.BL["req_date"])):
            if self.BL["req_date"][i] not in self.allres.keys():
                self.allres[self.BL["req_date"][i]] = 0
            self.allres[self.BL["req_date"][i]] += self.BL["ac_num"][i]
            

    def make_ymd(self,tt):
        nt = "%04d/%02d/%02d-%02d:%02d:%02d"%(tt[0],tt[1],tt[2],
                                              tt[3],tt[4],tt[5])
        return nt

    def make_ymd_today(self,tt):
        nt = "%04d/%02d/%02d-00:00:00"%(tt[0],tt[1],tt[2])
        return nt

    def make_ymd_nextday(self,tt):
        tt.split(":")
        nt = "%04d/%02d/%02d-00:00:00"%(tt[0],tt[1],tt[2])
        return nt

    def set_cookie(self):
        print('<script language="javascript">')
        print('document.cookie = "baotickethashid=%s; max-age=36000";'%self.hid)
        print('</script>')
        return 0

     
    def get_status(self):
        self.read_current()
        self.countup_allreservation()
        self.ex_d = self.read_bnum()
        self.st   = self.form.getvalue("date")        
        if self.st is None:
            self.st = self.make_ymd_today(time.localtime())
        self.JL = {}
        #self.settime = sorted(list(set(self.BL["req_date"])))
        self.settime = sorted(list(set(self.ex_d.keys())))
        for st in self.settime:
            if st not in self.allres.keys():self.allres[st] = 0
            if st not in self.ex_d.keys():self.ex_d[st] = self.defoult_max_num
            for i in range(len(self.BL["req_date"])):
                if st == self.BL["req_date"][i]:
                    if st not in self.JL.keys():self.JL[st] = []
                    v = {}
                    for k in self.keys:
                        if k == "hashid":continue
                        v[k] = self.BL[k][i]
                    self.JL[st].append(v)

    def get_status_today(self):
        self.read_current()
        self.countup_allreservation()
        tmp_ex_d  = self.read_bnum()
        self.st   = self.make_ymd_today(time.localtime()).split("-")[0]
        self.ex_d = {}
        self.JL   = {}
        for d in tmp_ex_d.keys():
            if d.split("-")[0] == self.st:self.ex_d[d] = tmp_ex_d[d]
            if d not in self.allres:self.allres[d] = 0
        self.settime = sorted(list(set(self.ex_d.keys())))
        for date in self.ex_d:
            for i in range(len(self.BL["req_date"])):
                if date == self.BL["req_date"][i]:
                    if date not in self.JL.keys():self.JL[date] = []
                    v = {}
                    for k in self.keys:
                        if k == "hashid":continue
                        v[k] = self.BL[k][i]
                    self.JL[date].append(v)
                    
def show_error():
    print("Content-type: text/html\n\n")
    print("<font color='white'>エラーが発生しました。お時間をおいて再度お試しください。<br>")
    print("<a href='https://www.bao.city.ibara.okayama.jp/?page_id=59'>")
    print("お問い合わせ先はこちら</a></font><br>")
    print('<button type="button" onclick="history.back()">戻る</button>')
        
if __name__ == "__main__":
    sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')
    try:
        BT = BAO_ticket()
        if BT.mode == "write":
            BT.show_html_header()
            if BT.numflag and BT.timeflag and BT.endtimeflag:
                if BT.reg:
                    BT.change_reservation()
                    BT.show_change_reservation()
                else:
                    if BT.num == 0:
                        BT.show_enter_number()
                    else:
                        BT.write_new_reservation()
                        BT.show_new_reservation()
            else:
                if BT.timeflag == False:
                    BT.show_time_limit()
                elif BT.endtimeflag == False:
                    BT.show_end_limit()
                elif BT.num == 0 and BT.reg == False:
                    BT.show_enter_number()
                else:
                    BT.show_num_limit()
        #elif BT.mode == "status-all":
        #    BT.get_status()
        #    BT.show_json()
        #elif BT.mode == "status":
        #    BT.get_status_today()
        #    BT.show_json()
        elif BT.mode == "confirm":
            BT.show_html_header()
            if BT.reg:
                BT.check_reservation()
                BT.show_reservation()
            else:
                BT.show_no_reservation()
    except:
        show_error()
    BT.remove_lock()
