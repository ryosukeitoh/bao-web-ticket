#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json,time
import cgi
import codecs
from http import cookies
import io,sys,os
import hashlib

sys.path.append("/usr/lib/cgi-bin/")

import entrance_ticket

def read_schedule(ffile):
    r = open(ffile,"r")
    k = r.read()
    r.close()
    return k

def show_html_header():
    print("Content-type: text/html\n\n")
    print('<html><meta charset=utf-8><head>')
    print("<title>電子整理券管理システム</title>")
    print("</head><body>")
    
def show_html_end():
    print('<br><a href="/ticket/controll_ticket.html">Back</a>')
    print("</body></html>")


def show_state(L):
    print('<h3>整理券発行スケジュール</h3>')
    print('YYYY/MM/DD-hh:mm:ss number_of_limit<br>')
    print('<form action="ticket_force_write.py" method="post" enctype="multipart/form-data">')
    print("<input type='hidden' name='mode' value='schedule'>")
    print('<textarea name="schedule" rows="20" cols="40">%s</textarea><br>'%L)
    print('<input type="submit" value="upload"></form>')
    
if __name__ == "__main__":
    sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')
    BT   = entrance_ticket.BAO_ticket()
    rL   = read_schedule(BT.dfile)
    show_html_header()
    show_state(rL)
    show_html_end()
    BT.remove_lock()
