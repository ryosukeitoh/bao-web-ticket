#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json,time
import cgi
import codecs
from http import cookies
import io,sys,os
import hashlib

sys.path.append("/usr/lib/cgi-bin/")

import entrance_ticket



def show_html_header():
    print("Content-type: text/html\n\n")
    print("<html><meta charset=utf-8><head>")
    print("<title>電子整理券管理システム</title>")
    print("</head><body>")
        
def show_html_end():
    print("</body></html>")


def show_current(L):
    return 0

if __name__ == "__main__":
    sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')
    BT = entrance_ticket.BAO_ticket()
    BT.get_status()
    show_html_header()
    print("<table>")
    for d in BT.settime:
        print("<tr><td> </td></tr>")
        print("<tr><td> </td></tr>")
        print("<tr><td> </td></tr>")
        print("<tr><td> </td></tr>")
        print("<tr><td colspan='5' align=center><b>%s: 計 %s/%s</b></td></tr>"%(d,
                                                                                 BT.allres[d],
                                                                                 BT.ex_d[d]))
        print("<tr><th>ID</th><th>人数</th><th>Check-In</th><th>Cancel</th></tr>")
        for L in BT.JL[d]:
            if   L["checkin"]  == 1:fc="tan"
            elif L["canceled"] == 1:fc="tan"
            else:                   fc="blue"
            print("<tr>")
            print("<td align=center><font color=%s>%s</font></td>"%(fc,L["ID"]))
            print("<td align=center><font color=%s>%s</font></td>"%(fc,L["ac_num"]))
            print("<td align=center><font color=%s>%s</font></td>"%(fc,L["checkin"]))
            print("<td align=center><font color=%s>%s</font></td>"%(fc,L["canceled"]))
            print("<td align=center><a href=''>check-in</a></td>")
            print("</tr>")
    print("</table>")
    show_html_end()
